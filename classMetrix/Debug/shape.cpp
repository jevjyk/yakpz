#include<afxwin.h>
#include<stdio.h>
#include<time.h>

class Shape
{
public:
	bool		cells[4][4]; 
	FigType	curType;

	void	NewFig( FigShape ); 
	void	Rotate();
	void	ClearShape( bool c[4][4] ); // clear the 4x4 matrix
};


void Shape::ClearShape( bool c[4][4] )
{
	
	for( int i=0; i<4; i++)
		for( int j=0; j<4; j++)
			c[i][j]=false;
			ClearShape();
}



class Child1 : public Shape
{
	void	NewShape( ShapeType ); 
	void	Rotate();
	void	ClearShape( bool c[4][4] ); // clear the 4x4 matrix
}


class Child2 : public Child1
{
	void	NewSshape( ShapeType ); 
	void	Rotate();
	void	ClearShape( bool c[4][4] ); // clear the 4x4 matrix
}




class Child3 : public Child1
{
	void	NewShape( Figshape ); 
	void	Rotate();
	void	ClearShape( bool c[4][4] ); // clear the 4x4 matrix
}
