#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_openFileAction_triggered();

    void on_pbuttonLinesNum_clicked();

    void on_pushButton_2_clicked();

    void on_pbuttonEmptyLinesNum_clicked();

    void on_pbuttonCommentsNum_clicked();

    void on_pbuttonOperandsListSize_clicked();

    void on_pbuttonOperands_clicked();

    void on_pushButton_8_clicked();

    void on_pbuttonOperatorsList_clicked();

    void on_pbuttonOperatorsNum_clicked();

    void on_pbuttonCyclicCompexity_clicked();

    void on_pbuttonVocabulary_clicked();

    void on_pbuttonProgramLength_clicked();

    void on_pbuttonGilbsCompexity_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
