#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>

int R = 0, operators_num = 0,
    operands_num = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openFileAction_triggered()
{
    QString openFileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("C++ code files (*.cpp)"));
    QFile file(openFileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    else
    {
        ui->plainTextCode->setPlainText(file.readAll());
    }
}

void MainWindow::on_pbuttonLinesNum_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    int lines_number = code_text.count("\n");
    ui->labelLinesNum->setText(QString::number(lines_number + 1));
}

void MainWindow::on_pbuttonEmptyLinesNum_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    int empty_lines_number = code_text.count("\n\n");
    ui->labelEmptyLinesNum->setText(QString::number(empty_lines_number));
}

void MainWindow::on_pbuttonCommentsNum_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    int comments_num = code_text.count("\n//");
    ui->labelCommentsNumber->setText(QString::number(comments_num));

}

void MainWindow::on_pbuttonOperandsListSize_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    QRegExp regEx("([\\w\\']+)[\\s,.;]");
    QStringList list;
    int pos = 0;
    while((pos = regEx.indexIn(code_text, pos))!= -1)
    {
        list << regEx.cap(1);
        pos += regEx.matchedLength();
    }
    list.removeDuplicates();
    QString types = "int void string double float iterator List const array for while do goto";
    QStringList Itypes;
    Itypes = types.split(" ");
    QStringList::Iterator itt = Itypes.begin();
    for (int i = list.count()-1; i >= 0; --i)
    {
        const QString &item = list[i];
        itt = Itypes.begin();
        while(itt != Itypes.end())
        {
            if (item == *itt)
                list.removeAt(i);
            ++itt;
        }
        if ((item.toFloat() != 0))
        {
            list.removeAt(i);
        }
    }

    for(int i = list.count()-1; i >= 0; --i)
    {
        const QString &item = list[i];
        if(item == "0")
        {
            list.removeAt(i);
        }
    }
    operands_num = list.count();
    ui->labelOperandsListSize->setText(QString::number(operands_num));
}

void MainWindow::on_pbuttonOperands_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    QRegExp regEx("([\\w\\']+)[\\s,.;]");
    QStringList list;
    int pos = 0;
    while((pos = regEx.indexIn(code_text, pos)) != -1)
    {
        list<<regEx.cap(1);
        pos += regEx.matchedLength();
    }
    list.removeDuplicates();
    QString types = "int void string double float iterator List const array for while do goto";
    QStringList Itypes;
    Itypes = types.split(" ");
    QStringList::Iterator itt = Itypes.begin();
    for (int i = list.count()-1; i >= 0; --i)
    {
        const QString &item = list[i];
        itt = Itypes.begin();
        while(itt != Itypes.end())
        {
            if (item==*itt)
                list.removeAt(i);
          ++itt;
        }
        if((item.toFloat()!=0))
        {
            list.removeAt(i);
        }
    }

    for (int i = list.count()-1; i>= 0; --i)
    {
        const QString &item = list[i];
        if(item == "0")
        {
            list.removeAt(i);
        }
    }
    int operands_num = list.count();
    ui->labelOperandsCount->setText(QString::number(operands_num));
}


void MainWindow::on_pbuttonOperatorsList_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    QStringList list = code_text.split(" ");
    QString types="+ - = += ++ -- * << >> < > != == || && &";
    QStringList lst = code_text.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"), QString::SkipEmptyParts);
    QStringList Itypes;
    QStringList ress;
    list.removeDuplicates();
    Itypes = types.split(" ");
    QStringList::Iterator itt = Itypes.begin();
    for (int i=list.count()-1; i >= 0; --i)
    {
        const QString &item = list[i];
        itt = Itypes.begin();
        while(itt!=Itypes.end())
        {
            if(item == *itt)
            {
                ress << item;
            }
            ++itt;
        }
    }
    int r = 0;
    QStringList::iterator t = lst.begin();
    for (int i = lst.count()-1; i >= 0; --i)
    {
        const QString &item = lst[i];
        r = r + item.count(")\n");
    }
    R = r;
    operators_num = ress.count();
    ui->labelOperatorsListSize->setText(QString::number(operators_num + r));
}

void MainWindow::on_pbuttonOperatorsNum_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    QStringList list = code_text.split(" ");
    QString types = "+ - = += ++ -- * << >> < > != == || && &";
    QStringList Itypes;
    QStringList ress;
    Itypes = types.split(" ");
    QStringList::Iterator itt = Itypes.begin();
    for (int i = list.count()-1; i >= 0; --i)
    {
        const QString& item=list[i];
        itt=Itypes.begin();
        while (itt!=Itypes.end() )
        {
            if(item==*itt)
            {
                ress<<item;
            }
            ++itt;
        }
    }
    operators_num = ress.count();
    ui->labelOperatorsNum->setText(QString::number(operators_num+R));
}

void MainWindow::on_pbuttonCyclicCompexity_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    QStringList list = code_text.split(QRegExp("(for|while)"),QString::SkipEmptyParts);
    QStringList::Iterator it=list.begin();
    int s = 0;
    for (int i = list.count()-1; i >= 0; --i)
    {
        const QString& item = list[i];
        if((item.count("{") >= item.count("}")) && (item.count("}") != 0))
        {
            s = s + 1;
        }
    }
    ui->labelCyclicComplexity->setText(QString::number(s + code_text.count("if(")));

}

void MainWindow::on_pbuttonVocabulary_clicked()
{
    int vocabulary_size = operands_num + operators_num + R;
    ui->labelVocabulary->setText(QString::number(vocabulary_size));
}

void MainWindow::on_pbuttonProgramLength_clicked()
{
    int program_length = operands_num + operators_num + 2;
    ui->labelProgramLength->setText(QString::number(program_length));
}

void MainWindow::on_pbuttonGilbsCompexity_clicked()
{
    QString code_text = ui->plainTextCode->toPlainText();
    ui->labelGilbsComplexity->setText(QString::number(code_text.count("if(")));
}
